import * as COMMON from '../constants/common';
import * as WINNERS from '../constants/winners';

const initState = {
	winners: [],
	loaded: false,
};

export default function (state = initState, action) {
	switch (action.type) {
		case COMMON.REQUEST_SENT:
			return {
				...state,
				...{
					loaded : false
				}
			};

		case WINNERS.WINNERS_FETCH:
			return {
				...state,
				...{
					winners: action.data,
					loaded: true
				}
			};


		case WINNERS.WINNERS_ADD:
			return {
				...state,
				...{
					winners : action.data,
					loaded : true,
				}
			};

		default:
			return state
	}
}
