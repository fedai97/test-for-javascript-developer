import * as COMMON from "../constants/common";
import * as WINNERS from "../constants/winners";
import WinnersApi from "../../services/api-worker/WinnersApi";

export function getWinners() {
	return function (dispatch) {
		dispatch({
			type: COMMON.REQUEST_SENT
		});
		(new WinnersApi()).getWinners().then(res => {
			return dispatch({
				type: WINNERS.WINNERS_FETCH,
				data: res
			});
		});
	};
}
export function addWinners(payload){
	return function (dispatch) {
		dispatch({
			type: COMMON.REQUEST_SENT
		});
		(new WinnersApi()).addWinners(payload).then(res => {
			return dispatch({
				type: WINNERS.WINNERS_ADD,
				data: res,
			});
		})
	};
}
